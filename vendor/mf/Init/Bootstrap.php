<?php

namespace mf\Init;

abstract class Bootstrap{
    private $routes;

    abstract protected function initRouts();

    function __construct(){
        $this->initRouts();
        $this->run($this->getURL());

    }

    function getRoutes(){
        return $this->routes;
    }
    function setRoutes(array $routes){
        $this->routes = $routes;
    }
    protected function run($url){

        foreach($this->getRoutes() as $key => $route){
            if($url == $route['route']){
                $class = "app\\Controllers\\".ucfirst($route['controller']);

                $controller = new $class;
                $action = $route['action'];
                $controller->$action();
            }
            
        }
    }

    protected function getURL(){ 

        return parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH );
    }
}


?>