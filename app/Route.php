<?php

namespace app;

use mf\Init\Bootstrap;

class Route extends Bootstrap{

    protected function initRouts(){
        $routes['home'] = array(
            'route' => '/',
            'controller' => 'indexController',
            'action' => 'index'
        );
      

        $this->setRoutes($routes);

    }
}
?>