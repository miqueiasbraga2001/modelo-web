# Modelo WEB

Modelo base para desenvolvimento utilizando padrão [MVC](https://www.lewagon.com/pt-BR/blog/o-que-e-padrao-mvc)

## Linguagens:

#### Front-end
- HTML
- CSS 
- SASS
- JavaScript

#### Back-end
- PHP
- MySQL

## Frameworks
- [Bootstrap](https://blog.getbootstrap.com/2022/05/13/bootstrap-5-2-0-beta/)
- [Font awesome](https://fontawesome.com)
- [Jquery](https://jquery.com)
- [Ajax](https://developer.mozilla.org/pt-BR/docs/Web/Guide/AJAX)
